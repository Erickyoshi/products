package br.edu.unisep.products.domain.validator;

import org.apache.commons.lang3.Validate;

public class FindProductByIdValidator {

    public void validate(Integer id){
        Validate.isTrue(id>0, "Informe o id do produto!");
    }
}
