package br.edu.unisep.products.domain.usecase;

import br.edu.unisep.products.data.dao.ProductDao;
import br.edu.unisep.products.domain.dto.ProductDto;
import br.edu.unisep.products.domain.validator.FindProductByIdValidator;

public class FindProductByIdUseCase {

    public ProductDto execute(Integer id){

        var validator = new FindProductByIdValidator();
        validator.validate(id);

        var dao = new ProductDao();
        var product = dao.findById(id);

        if(product != null){
            return new ProductDto(
                    product.getId(),
                    product.getName(),
                    product.getDescription(),
                    product.getPrice(),
                    product.getBrand(),
                    product.getStatus(),
                    product.getStatus2());
        }
        return null;
        }

    }

