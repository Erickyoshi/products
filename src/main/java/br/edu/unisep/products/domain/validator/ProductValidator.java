package br.edu.unisep.products.domain.validator;

import br.edu.unisep.products.domain.dto.RegisterProductDto;
import org.apache.commons.lang3.Validate;

public class ProductValidator {

    public void valdiate(RegisterProductDto registerProduct){
        Validate.notBlank(registerProduct.getName(), "Informe o nome do produto!");
        Validate.notBlank(registerProduct.getDescription(), "Informe a descrição do produto!");
        Validate.notNaN(registerProduct.getPrice(), "Informe o preço do produto!");
        Validate.notBlank(registerProduct.getBrand(), "Informe a marca do produto!");
        Validate.notNaN(registerProduct.getStatus(), "Informe o status do produto!");
        Validate.isTrue(registerProduct.getPrice()>0, "Informe um preço válido!");
        Validate.isTrue(registerProduct.getStatus()>2, "Informe um status válido, (0, 1, 2)");
    }
}
