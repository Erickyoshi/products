package br.edu.unisep.products.domain.builder;

import br.edu.unisep.products.data.entity.Product;
import br.edu.unisep.products.domain.dto.ProductDto;
import br.edu.unisep.products.domain.dto.RegisterProductDto;

import java.util.List;
import java.util.stream.Collectors;

public class ProductBuilder {


    public List<ProductDto> from(List<Product> products){
        return products.stream().map(this::from).collect(Collectors.toList());
    }

    public ProductDto from (Product product){
        if(product!=null){
            return new ProductDto(
                    product.getId(),
                    product.getName(),
                    product.getDescription(),
                    product.getPrice(),
                    product.getBrand(),
                    product.getStatus(),
                    product.getStatus2());
        }
        return null;
    }

    public Product from(RegisterProductDto registerProduct) {
        var product = new Product();
        product.setName(registerProduct.getName());
        product.setDescription(registerProduct.getDescription());
        product.setPrice(registerProduct.getPrice());
        product.setBrand(registerProduct.getBrand());
        product.setStatus(registerProduct.getStatus());
        product.setStatus2(registerProduct.getStatus2());

        return product;
    }
}
