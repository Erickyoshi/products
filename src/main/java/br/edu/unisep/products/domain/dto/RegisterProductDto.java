package br.edu.unisep.products.domain.dto;

import lombok.Data;

@Data
public class RegisterProductDto {

    private String name;
    private String description;
    private Double price;
    private String brand;
    private Integer status;
    private String status2;

}
