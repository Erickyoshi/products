package br.edu.unisep.products.domain.usecase;

import br.edu.unisep.products.data.entity.Product;
import br.edu.unisep.products.domain.dto.RegisterProductDto;
import br.edu.unisep.products.domain.validator.ProductValidator;

public class RegisterProductUseCase {

    public void execute(RegisterProductDto registerProduct){
        var validator =new ProductValidator();
        validator.valdiate(registerProduct);

        var product = new Product();
        product.setName(registerProduct.getName());
        product.setDescription(registerProduct.getDescription());
        product.setPrice(registerProduct.getPrice());
        product.setBrand(registerProduct.getBrand());
        product.setStatus(registerProduct.getStatus());
        product.setStatus2(registerProduct.getStatus2());

    }
}
