package br.edu.unisep.products.domain.usecase;

import br.edu.unisep.products.data.dao.ProductDao;
import br.edu.unisep.products.domain.builder.ProductBuilder;
import br.edu.unisep.products.domain.dto.ProductDto;

import java.util.List;
import java.util.stream.Collectors;

public class FindAllProductsUseCase {

    public List<ProductDto> execute(){
        var dao = new ProductDao();
        var products = dao.findAll();

        var builder= new ProductBuilder();
        builder.from(products);

        return products.stream().map(product -> {
            if(product.getStatus() == 0){
                product.setStatus2("Ativo");
            }
            if(product.getStatus() == 1){
                product.setStatus2("Inativo");
            }
            if(product.getStatus() == 2){
                product.setStatus2("Suspenso");
            }
            return new ProductDto(
                    product.getId(),
                    product.getName(),
                    product.getDescription(),
                    product.getPrice(),
                    product.getBrand(),
                    product.getStatus(),
                    product.getStatus2());
        }).collect(Collectors.toList());
    }
}
