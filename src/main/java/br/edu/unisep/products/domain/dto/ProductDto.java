package br.edu.unisep.products.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ProductDto {

    private final Integer id;

    private final String name;

    private final String description;

    private final Double price;

    private final String brand;

    private final Integer status;

    private final String status2;

}
