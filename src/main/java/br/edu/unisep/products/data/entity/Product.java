package br.edu.unisep.products.data.entity;

import lombok.Data;
import lombok.Getter;
import javax.persistence.*;

@Getter
@Data
@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private Double price;

    @Column(name = "brand")
    private String brand;

    @Column(name = "status")
    private Integer status;

    @Column(name = "status2")
    private String status2;
}
